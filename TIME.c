#include<stdio.h>

int convert_to_min(int hours, int minutes)
{
	int total;
	total = (hours * 60) + minutes;
	return total;
}

int main()
{
	int hrs, minutes, result;
	printf("enter the hours\n");
	scanf("%d", &hrs);
	printf("enter the minutes\n");
	scanf("%d", &minutes);
	result = convert_to_min(hrs, minutes);
	printf("minutes=%d\n", result);
	return 0;
}